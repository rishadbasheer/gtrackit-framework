package com.zaeem.gtrackit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zaeem.gtrackit.dao.Car;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer>{

}
