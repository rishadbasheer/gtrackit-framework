package com.zaeem.gtrackit.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zaeem.gtrackit.dto.CarDto;
import com.zaeem.gtrackit.service.CarService;

@Controller
@RequestMapping(path = {"/apis/v1"})
public class CarController {
	@Autowired
	private CarService carService;
	@GetMapping(value = "/cars")
	public ResponseEntity<List<CarDto>> getCars() {
		List<CarDto> cars = new ArrayList<CarDto>();
		cars = carService.getCars();
		return new ResponseEntity<List<CarDto>>(cars,HttpStatus.OK);
	}

}
