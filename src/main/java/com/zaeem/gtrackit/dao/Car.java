package com.zaeem.gtrackit.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "car")
public class Car {
@Id
@Column(name = "id")
private Integer id;
@Column(name = "contractid")
private String contractId;
@Column(name = "site")
private String site;
@Column(name = "plate")
private String plate;
@Column(name = "driverid")
private Integer driverId;
/**
 * @return the id
 */
public Integer getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(Integer id) {
	this.id = id;
}
/**
 * @return the contractId
 */
public String getContractId() {
	return contractId;
}
/**
 * @param contractId the contractId to set
 */
public void setContractId(String contractId) {
	this.contractId = contractId;
}
/**
 * @return the site
 */
public String getSite() {
	return site;
}
/**
 * @param site the site to set
 */
public void setSite(String site) {
	this.site = site;
}
/**
 * @return the plate
 */
public String getPlate() {
	return plate;
}
/**
 * @param plate the plate to set
 */
public void setPlate(String plate) {
	this.plate = plate;
}
/**
 * @return the driverId
 */
public Integer getDriverId() {
	return driverId;
}
/**
 * @param driverId the driverId to set
 */
public void setDriverId(Integer driverId) {
	this.driverId = driverId;
}


}
