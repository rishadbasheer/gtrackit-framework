package com.zaeem.gtrackit.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import com.zaeem.gtrackit.dao.Car;
import com.zaeem.gtrackit.dto.CarDto;

@Mapper
public class CarMapper {

	public static List<CarDto> mapCarToCarDto(List<Car> cars, List<CarDto> carsResult) {
		List<CarDto> carsMapped = new ArrayList<CarDto>();
		for(Car car : cars) {
			CarDto carDto = new CarDto();
			carDto.setId(car.getId());
			carDto.setContractId(car.getContractId());
			carDto.setDriverId(car.getDriverId());
			carDto.setPlate(car.getPlate());
			carDto.setSite(car.getSite());
			carsMapped.add(carDto);
		}
		return carsMapped;
	}

}
