package com.zaeem.gtrackit.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zaeem.gtrackit.dao.Car;
import com.zaeem.gtrackit.dto.CarDto;
import com.zaeem.gtrackit.mapper.CarMapper;
import com.zaeem.gtrackit.repository.CarRepository;

@Service
@Transactional
public class CarService {
	private static final Logger logger = LoggerFactory.getLogger(CarService.class);
    @Autowired
    private CarRepository carRepository;
	public List<CarDto> getCars() {
		logger.info("CarService starts...");
		List<CarDto> carsResult = new ArrayList<CarDto>();
		List<Car> cars = carRepository.findAll();
		carsResult = CarMapper.mapCarToCarDto(cars,carsResult);
		return carsResult;
	}

}
